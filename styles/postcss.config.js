module.exports = {
  map: { 
    inline: false,
    annotation: true,
    sourcesContent: true
  },
  plugins: {
    "postcss-import-url": {
    },
    autoprefixer: {
      cascade: false
    },
  }
}
